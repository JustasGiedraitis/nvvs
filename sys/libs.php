<?php

class header_footer_ext {

    private $libraries = [];
    
    function add_library($lib) {
        $this->libraries[$lib] = true;
	}
    
    
    function load_libs()
    {
         if($this->libraries['reportModal'])
            $this->reportModal();
        
    }
    
    
    
    
    
    
    function reportModal()
    {
        ?>
        <!-- Modal -->
        <div class="modal " id="modalReportDiv" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle">Loading ...</h5> <!-- Duomenų ataskaita -->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body"></div>
                    
                    <div class="modal-footer">
                    
                        <span id="left-part" style='left:0; position: relative;' ></span>
                        <span style='float:right; display:inline-block; position:relative; margin-right:4px;' id="right-part"></span>

                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/search.js"></script>
        <?php
    }
    
    
    
    
    
}

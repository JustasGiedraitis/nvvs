<?php

require_once 'libs.php';


class __head_footer_loader extends header_footer_ext {

	private $page_loaded_miliseconds;

	function __construct(){ // Header *****************************************
		$this->page_loaded_miliseconds = microtime(true);
		?><!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/style.css">

    <script type="text/javascript" src="/DOM.js" ></script>
    <script type="text/javascript" src="/sys/addLine/addLine.js"></script>

</head>
<body>
		<?
	}

	
	


	function __destruct(){ // Footer *****************************************
?>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<script src="https://momentjs.com/downloads/moment.min.js"></script>
	<script src="/assets/calendar.js"></script>

    <?php
        $this->load_libs();
    
        echo "<script> console.log('page loaded: ". round((microtime(true) - $this->page_loaded_miliseconds) , 6) .' \')</script>';
		
		if($_SESSION['sql_miliseconds']) {
			echo "<script> console.log('sql loaded: ". round( $_SESSION['sql_miliseconds'], 6 ) .' \')</script>';
		}

        if($_SESSION['sql_errors']){
            dump($_SESSION['sql_errors']);
            $_SESSION['sql_errors'] = null;
        }
    ?>

</body>
</html>
<?
    }

}

$__head_footer_loader = new __head_footer_loader();

    <input type="hidden" name="__token"             value="<?= getToken() ?>" >
            <input type='hidden' name='0[settings][main]'   value='1' >
            <input type='hidden' name='0[settings][table]'  value='<?= $table ?>' >
            <input type='hidden' name='0[settings][id_row]' value='<?= $id_row ?>' >
            <input type='hidden' name='0[settings][id]'     value='<?= $_REQUEST[$id_row] ?>' >

            <?php if($result === false): ?>
                <input type='hidden' name='0[settings][new]' value='1' ><?php endif ?>

            <div class="row" >
<?php
unset($result[ $id_row ]);
foreach($pattern as $column) {
    echo "
				 <!-- --- --- --- --- row --- --- --- --- -->
				 <div class=\"col-xl-2  col-md-4  col-sm-6\">";
    foreach($column as $key => $params)
    {
        $row_name = $params['row'];
        $row_value = $result[$row_name];

        $type = "text";
        $label = "";
        $search = "";
        if($params['par']) {
            if($params['par']['type'])
                $type = $params['par']['type'];
            if($params['par']['label'])
                $label = $params['par']['label'];
            if($params['par']['search'])
                $search = $params['par']['search'];

        }

        echo '
					<div class="form-group-sm">
						<label for="id__'.$row_name.'">'.$label.'</label>
							<div class="input-group mb-3">
							  <input type="'.$type.'" name="0['. $row_name .']" id="id__'.$row_name.'" value="'. rmEmpty($row_value). '" column="'.$row_value.'" '.($search ? "onfocusin='search(this,\"".$search."\")' " : '').' class="form-control form-control-sm" aria-label="" aria-describedby="basic-addon1" ' .($search ? 'autocomplete="off"':''). '>';

        if($search){
            echo '
								
									<!-- extra -->
									<div class="input-group-append">
										<button class="btn btn-outline-secondary" type="button" onclick="reportModal.show(\''.$search.'\')">
											<img src="assets/svg/list.svg" class="icon">
										</button>
									</div>';
        }

        echo '
							</div>
					</div>
				';
    }
    echo "</div>\n\t\t\t\t<!-- end column -->\n";
}
echo "\n";
?>

            </div><!-- end row -->

<?php
	// echo $_SERVER['SCRIPT_NAME'];





	function insertUpdateDelete($value, $andWhere = []) 
	{
		global $db;
		$settings = $value['settings'];
			unset($value['settings']);

			if($settings['delete']) {

                if( !empty($settings['id_row']) && !empty($settings['id']) ) {

                    $table = $settings['table'];
                    if (
                        strpos($table, '"') !== false ||
                        strpos($table, "'") !== false ||
                        strpos($settings['id_row'], '"') !== false ||
                        strpos($settings['id_row'], "'") !== false
                    ) {
                        $_SESSION['sql_errors'][] = "Rastos kabutės!";
                        return;
                    }

                    $sth = $db->prepare("delete from `$table` where `{$settings['id_row']}` = :id ");
//                    $sth->bindValue(':row_name', $settings['id_row']);
                    $sth->bindValue(':id', $settings['id']);
                    $sth->execute();
                    $err = $db->errorInfo();

                    if ($err[2])
                        $_SESSION['sql_errors'][] = $err;
                }

                return;
			}

			if(isset($settings['id']) && !isset($settings['new'])) {

				$query = 'update `'.$settings['table'].'` set '.prepareUpdate($value) .' where `'.$settings['id_row'].'` = :'.$settings['id_row'];
				
				// i gala prideda sql uzklausa
				if($andWhere) {
				    foreach($andWhere as $k => $ex){
                        $query .= ' and `'.$k.'` = :'  . $k;
                    }
                }


				$sth = $db->prepare($query);
				$sth->bindValue(':'.$settings['id_row'], $settings['id']);
                $id = $settings['id'];

				foreach ($value as $key => $value) {
					if(!is_array($value)){
						$sth->bindValue(':'.$key, $value);
					}
				}
                if($andWhere) {
                    foreach ($andWhere as $k => $ex) {
                        $sth->bindValue(':'.$k, $ex);
                    }
                }

				$sth->execute();

			} else {
				if($settings['id'] > 0 && $settings['table']) {
					$value[$settings['id_row']] = $settings['id'];
				}

                if($andWhere) {
                    foreach ($andWhere as $k => $ex) {
                        $value[$k] = $ex;
                    }
                }

				$query = 'insert into `'.$settings['table'].'` '. prepareInsert($value);
				$sth = $db->prepare($query);
				foreach ($value as $key => $value) {
					if(!is_array($value)){
						$sth->bindValue(':'.$key, $value);
					}
				}
				$sth->execute();
				$id = $db->lastInsertId();
                $err = $db->errorInfo();

                if($err[2]) {
                    $_SESSION['sql_errors'][] = $err;
                }

				if($settings['main'] && !$_GET[$settings['id_row']] ){
					$_GET[$settings['id_row']] = $id;
					$location = '?'. http_build_query($_GET);
				}
			}
		return [ 'redirect' => $location, 'lastInsertId' => $id ];
	}


if(!empty($_POST['__token'])) {
    if(isTokenValid($_POST['__token'])) {
        unset($_POST['__token']);

		$_SESSION['sql_miliseconds'] = microtime(true);

        foreach ($_POST as $key => $value) {
            if(isset($value['settings']))
            {
                $extra = [];
                if($value['extra']){
                    $extra = $value['extra'];
                    unset($value['extra']);
                }
                $lc = insertUpdateDelete($value);
                if(isset($lc['redirect']))
                    $location = $lc['redirect'];
                if(isset($lc['lastInsertId'])) {

                    foreach($extra as $ext) {
                        $andWhere = [];
                        if($ext['settings']['global_id_row']) {

                            if($lc['settings']['global_id']) {
                                $andWhere = [
                                    "{$ext['settings']['global_id_row']}"
                                    => $lc['settings']['global_id']
                                ];
                            } else {
                                $andWhere = [
                                    "{$ext['settings']['global_id_row']}"
                                    => $lc['lastInsertId']
                                ];
                            }
                        }
                        insertUpdateDelete($ext, $andWhere);
                    }
                }
            }
        }


		$_SESSION['sql_miliseconds'] =  microtime(true) - $_SESSION['sql_miliseconds'];
    } else{
        // jei blogas tokenas
		$_SESSION['sql_miliseconds'] = null;
    }
} else {
	    // echo 'nerado token\'o ';
		$_SESSION['sql_miliseconds'] = null;
}






	if($location) {
		// dump($_SERVER['SCRIPT_NAME'] . $location );
		header('Location: '. $_SERVER['SCRIPT_NAME'] . $location );
	}

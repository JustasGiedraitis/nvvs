var _row_inx_ = 0;
var addLine = function(div, params, array) {
	var ln = 0;
	var options   = params.options;
	var structure = params.structure;

	function prepareLine(inx, row) {
		var table = '\n<tr>';

		if(options.global_id_row) {
			table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][global_id_row]" value="'+ options.global_id_row +'" >'
			if(options.global_id > 0)
			table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][global_id]" value="'+ options.global_id +'" >'
		}

		if(options.table) {
			table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][table]" value="'+ options.table +'" >'
			if(options.id_row) {
				table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][id_row]" value="'+ options.id_row +'">'
				if(row[options.id_row]){
					table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][id]" value="'+ row[options.id_row] +'">'
				} else {
					table += '\n<input type="hidden" name="'+ln+'[extra]['+inx+'][settings][new]" value="1">'
				}
			}
		}

        table += "<td>\n<input type='checkbox' name=\""+ln+"[extra]["+inx+"][settings][delete]\" value='1'>\n</td>"

		for(var i=0, il=structure.length; i<il; i++) {
			var struct = structure[i];
			var extra_class   = '';
            var column_name   = struct.row
            var column_params = struct.par
            var type = column_params.type || 'text'
			
			if (type == 'calendar') {
				type = 'text'
				extra_class = ' calendar ';
			}

			table += '\n<td>'
			if(! column_name)
				alert('labai rimta laika !')

			table += '\n       <input type="'+type+'" name="'+ln+'[extra]['+inx+']['+column_name+']" value="'+ (row[column_name] || '') +'" column="'+column_name+'" class="form-control form-control-sm '+ extra_class +'" >'
			if(column_params.rg) {
				// table += '<input type="hidden" name="'+ln+'[extra]['+inx+']['+column_name+'][rg]" value="1" >'
			}

			table += '</td>'
		}
		table += '\n</tr>'

		return table;
	}

	var table = $nElem('table').class('table table-bordered table-hover')


	var head_part = '<thead><tr>';
        head_part += "<th style='width:16px;'></th>"
	for (var i=0, il=structure.length; i<il; i++) {
        head_part += "<th style='"+ (structure[i].par.style || '') + "' >"+ (structure[i].par.label || '') +'</th>'
	}
    head_part += '</tr></thead>';


	var full_table = ''
	for(var i=0, il=array.length; i<il; i++) { 		
		full_table += prepareLine(_row_inx_, array[i])
        _row_inx_ ++;
	}
	div.append(table);
    table.append( head_part + full_table )

	var el = $nElem('button').attr({ type:'button' }).class('btn').html('Pridėti naują eilutę')
	div.parent().append(el)

	el.on('click',function(){
		var nLine = prepareLine(_row_inx_, {});

        table.append($(nLine)[0]);
        _row_inx_ ++;
	});
	
}

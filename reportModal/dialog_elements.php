<?php

$elem = $db->prepare(' SELECT COUNT(*) as total_rows FROM '. $table);
$elem->execute();
$total_rows = $elem->fetch(PDO::FETCH_ASSOC)['total_rows'];
$total_pages = ceil($total_rows/100);

?>
<div style="display:none;" id="pagination">
    <nav aria-label="...">
        <ul class="pagination">
		
            <li class="page-item <?= $page == 0 ? 'disabled' : '' ?>">
                <a class="page-link" href="#" tabindex="-1" i="<?= $page != 0 ? $page - 1 : -1 ?>" >Previous</a>
            </li>
			<?php
				for($ph=0; $ph<$total_pages; $ph++) {
					$current_page = $page == $ph ? 'active' : '';
					$current_span = $page == $ph ? '<span class="sr-only">(current)</span>' : '';
					echo '<li class="page-item '.$current_page.' ">'.
							'<a class="page-link" href="#" i="'.$ph.'" >'.($ph+1).$current_span.'</a>'.
						 '</li>';
				}
			?>
			<li class="page-item <?= $page == $total_pages-1 ? 'disabled' : '' ?>">
                <a class="page-link" href="#" i="<?= $page != $total_pages-1 ? $page + 1 : -1 ?>" >Next</a>
            </li>

        </ul>
    </nav>
</div>
<div style="display: none;" id="buttons" >
    <button type="button" class="btn btn-primary" id="new_item">Naujas įrašas</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
</div>


<script>
    window.parent.reportModal.onReady(function(getParts) {
        getParts.modalTitle.html('<?= $page_title ?>')

        var pagination = $id('pagination').cloneNode(true).css({ display: 'inline-block' })
        var page_item = pagination.findAll('li.page-item')

        for(var i=0, il=page_item.length; i<il; i++) {
            page_item[i].on('click',function(){
				var page = +this.find('a').attr('i')
				if(page != -1) {
					location.href = pageInfo().pathname + '?page=' + page
				}
            });
        }


        var buttons = $id('buttons').cloneNode(true).css({ display: 'inline-block' })
        buttons.find('#new_item').on('click',function(){
            location.href = pageInfo().pathname.replace('list.php','record.php') + '?page=<?= $_GET['page'] ?>'
        });

        getParts.modalDialog.find('#left-part').html('').append( pagination );
        getParts.modalDialog.find('#right-part').html('').append( buttons );
    });

    function openRecord(id) {
        location.href = pageInfo().pathname.replace('list.php','record.php') + '?id='+id+'&page=<?= $_GET['page'] ?>'
    }
</script>



<?php

	function th($order_by, $title) {
		global $request_w_get;
		$_GET['order_by'] = $order_by;
		$uri = $request_w_get . '?' . http_build_query($_GET);
		return "<th scope='col'><a href='". $uri ."'>".$title."</a></th>";
	}

?>
<?php
$dir = '../..';
include_once $dir.'/sys/head.php';
include_once $dir.'/sys/conn.php';
include_once $dir.'/sys/post_worker.php';

$page_title = "Duomenų ataskaita";
$table = 'cars';
$max_visible_rows = 100;
$page = +$_GET['page'];

$query = ' SELECT * FROM `cars` LIMIT '.($page * $max_visible_rows).', '.$max_visible_rows;
$elem = $db->prepare($query);
$elem->execute();
$result = $elem->fetchAll(PDO::FETCH_ASSOC);


require_once '../dialog_elements.php';
?>

<div class="container-fluid">

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
			<?= th('car_nr', 'Mašinos nr.') ?>
			<?= th('type', 'Mašinos tipas') ?>
        </tr>

        </thead>
        <tbody>
            <?php
                foreach($result as $row){
                    ?>
                        <tr style="cursor:pointer" onmousedown="openRecord('<?= $row['id'] ?>')" >
                            <th scope="row"><?= $row['id'] ?></th>
                            <td><?= $row['car_nr'] ?></td>
                            <td><?= $row['type'] ?></td>
                        </tr>
                    <?
                }
            ?>

        </tbody>
    </table>
</div>

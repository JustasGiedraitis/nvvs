<?php
$dir = '../..';
include_once $dir.'/sys/head.php';
include_once $dir.'/sys/conn.php';
include_once $dir.'/sys/post_worker.php';


$div_max_width = '800px';
?>

<div style="display:none;" id="pagination">
    <button type="button" class="btn btn-primary">Ištrinti</button>
</div>
<div style="display: none;" id="buttons">
<!--    <button type="button" class="btn btn-primary" id="saveChanges">Išsaugoti</button>-->
    <button type="button" class="btn btn-secondary" id="goBack" >Atgal</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
</div>


<script>
    var iframeGetParts
    window.parent.reportModal.onReady(function(getParts){
        iframeGetParts = getParts;

        getParts.modalTitle.html('Masšinos')

        var pagination = $id('pagination').cloneNode(true).css({ display: 'inline-block' })
        getParts.modalDialog.find('#left-part').html('').append( pagination );

        var buttons = $id('buttons').cloneNode(true).css({ display: 'inline-block' })
            // buttons.find('#saveChanges').on('click',saveChanges)
            buttons.find('#goBack').on('click',goBack)
        getParts.modalDialog.find('#right-part').html('').append( buttons );


        <? if($div_max_width ): ?>
        getParts.modalDialog.find('.modal-dialog').css({ maxWidth: '<?= $div_max_width  ?>' })
        <? endif ?>
    });

    function saveChanges() {
        $id('submitForm').submit();
    }
    function goBack() {
        <? if($div_max_width ): ?>iframeGetParts.modalDialog.find('.modal-dialog').css({ maxWidth: '' });
<? endif ?>
        location.href = pageInfo().pathname.replace('record.php','list.php') + ('<?= $_GET['page'] ?>' ? '?page=<?= $_GET['page'] ?>' : '')
    }
</script>

<div class="container-fluid">

    <?php
        $table = 'cars';
        $row = 'cars';
        $id_row = 'id';
        $elem = $db->prepare(' SELECT * FROM `'.$table.'` WHERE `'.$id_row.'` = :id ');
        $elem->bindParam(':id', $_REQUEST[$id_row]);
        $elem->execute();
        $result = $elem->fetch(PDO::FETCH_ASSOC);
    ?>

<form method='post' action="<?= $request ?>" id="submitForm">

        <?php
        $pattern = [];
        $pattern[] = [
            [
                'row' => 'car_nr',
                'par' => [ 'label' => 'Mašinos nr'  ],
            ]
        ];
        $pattern[] = [
            [
                'row' => 'type',
                'par' => [ 'label' => 'Mašinos tipas'  ],
            ]
        ];

        require_once $dir.'/sys/paprastas_formos_piesimas.php';

        ?>

    <br><br>
    <div style='display:block;text-align: center;'>
        <input type='submit' value='Išsaugoti pakeitimus' class='btn btn-primary'>
    </div>

</form>







</div>
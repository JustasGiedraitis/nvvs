<?php
$dir = '../..';
include_once $dir.'/sys/head.php';
include_once $dir.'/sys/conn.php';
include_once $dir.'/sys/post_worker.php';


$div_max_width = '800px';
?>

<div style="display:none;" id="pagination">
    <button type="button" class="btn btn-primary">Ištrinti</button>
</div>
<div style="display: none;" id="buttons">
<!--    <button type="button" class="btn btn-primary" id="saveChanges">Išsaugoti</button>-->
    <button type="button" class="btn btn-secondary" id="goBack" >Atgal</button>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Uždaryti</button>
</div>


<script>
    var iframeGetParts
    window.parent.reportModal.onReady(function(getParts){
        iframeGetParts = getParts;

        getParts.modalTitle.html('Vartotojai')

        var pagination = $id('pagination').cloneNode(true).css({ display: 'inline-block' })
        getParts.modalDialog.find('#left-part').html('').append( pagination );

        var buttons = $id('buttons').cloneNode(true).css({ display: 'inline-block' })
            // buttons.find('#saveChanges').on('click',saveChanges)
            buttons.find('#goBack').on('click',goBack)
        getParts.modalDialog.find('#right-part').html('').append( buttons );


        <? if($div_max_width ): ?>
        getParts.modalDialog.find('.modal-dialog').css({ maxWidth: '<?= $div_max_width  ?>' })
        <? endif ?>
    });

    function saveChanges() {
        $id('submitForm').submit();
    }
    function goBack() {
        <? if($div_max_width ): ?>iframeGetParts.modalDialog.find('.modal-dialog').css({ maxWidth: '' });
<? endif ?>
        location.href = pageInfo().pathname.replace('record.php','list.php') + ('<?= $_GET['page'] ?>' ? '?page=<?= $_GET['page'] ?>' : '')
    }
</script>

<div class="container-fluid">

    <?php
        $row = 'test_list';
        $id_row = 'id';
        $elem = $db->prepare(' SELECT * FROM `'.$row.'` WHERE `'.$id_row.'` = :id ');
        $elem->bindParam(':id', $_REQUEST[$id_row]);
        $elem->execute();
        $result = $elem->fetch(PDO::FETCH_ASSOC);
    ?>



<form method='post' action="<?= $request ?>" id="submitForm">
    <?php $token = getToken(); ?>
    <input type="hidden" name="__token" value="<?= $token ?>" >


    <input type='hidden' name='0[settings][main]'   value='1' >
    <input type='hidden' name='0[settings][table]'  value='<?= $row ?>' >
    <input type='hidden' name='0[settings][id_row]' value='<?= $id_row ?>' >
    <input type='hidden' name='0[settings][id]'     value='<?= $_REQUEST[$id_row] ?>' >

    <?php if($result === false): ?>
        <input type='hidden' name='0[settings][new]' value='1' >
    <?php endif ?>

    <div class="row" >

        <?php

        $pattern = [
            [	'vardas' => [ 'label' => 'Vardas' ],
                'pavarde' => [ 'label' => 'Pavardė' ],
                'adresas'  => [ 'label' => 'Adresas' ],
            ],
            [
                'vieta' => [ 'label' => 'Vieta' ],
                'a1' => [ 'label' => 'A1' ],
                'a2' => [ 'label' => 'A2' ],
            ],
            [
                'a3' => [ 'label' => 'A3' ],
                'a4' => [ 'label' => 'A4' ],
            ]
        ];


        unset($result['id']);
        foreach($pattern as $column) {
            echo "
                             <div class=\"col-xl-2  col-md-4  col-sm-6\">";
            foreach($column as $key => $params) {
                ?>

                <div class="form-group-sm">
                    <label for="id__<?= $key ?>"><?= $key ?></label>
                    <div class="input-group mb-3"> <!-- <input type="text" class="form-control form-control-sm" placeholder="" > -->
                        <input type="<?= isset($params['type']) ? $params['type'] : 'text' ?>" name="0[<?= $key ?>]" id="id__<?= $key ?>" value="<?= rmEmpty($result[$key])  ?>" column="<?= $key ?>" class="form-control form-control-sm" aria-label="" aria-describedby="basic-addon1">
                    </div>
                </div>

                <?php
            }
            echo '</div>';
        }
        ?>
    </div>


    <br>
    <br>
    <div style='display:block;text-align: center;'>
        <input type='submit' value='Išsaugoti pakeitimus' class='btn btn-primary'>
    </div>

</form>







</div>
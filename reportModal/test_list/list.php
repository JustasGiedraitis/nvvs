<?php
$dir = '../..';
include_once $dir.'/sys/head.php';
include_once $dir.'/sys/conn.php';
include_once $dir.'/sys/post_worker.php';

$page_title = "Duomenų ataskaita";
$table = 'test_list';
$max_visible_rows = 100;
$page = +$_GET['page'];

$elem = $db->prepare(' SELECT * FROM `test_list` LIMIT '.($page * $max_visible_rows).', '.$max_visible_rows);
$elem->execute();
$result = $elem->fetchAll(PDO::FETCH_ASSOC);


require_once '../dialog_elements.php';
?>

<div class="container-fluid">

    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
			<?= th('vardas', 'Vardas') ?>
			<?= th('pavarde', 'Pavardė') ?>
			
			<?= th('adresas', 'Adresas') ?>
			<?= th('vieta', 'Vieta') ?>
			<?= th('a1', 'a1') ?>
			<?= th('a2', 'a2') ?>
			<?= th('a3', 'a3') ?>
			<?= th('a4', 'a4') ?>
        </tr>

        </thead>
        <tbody>
            <?php
                foreach($result as $row){
                    ?>
                        <tr style="cursor:pointer" onmousedown="openRecord('<?= $row['id'] ?>')" >
                            <th scope="row"><?= $row['id'] ?></th>
                            <td><?= $row['vardas'] ?></td>
                            <td><?= $row['pavarde'] ?></td>
                            <td><?= $row['adresas'] ?></td>
                            <td><?= $row['vieta'] ?></td>
                            <td><?= $row['a1'] ?></td>
                            <td><?= $row['a2'] ?></td>
                            <td><?= $row['a3'] ?></td>
                            <td><?= $row['a4'] ?></td>
                        </tr>
                    <?
                }
            ?>

        </tbody>
    </table>
</div>

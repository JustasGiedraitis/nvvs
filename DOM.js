function gale00(n) {
    n = (Math.round(n*100)/100) + ''
    n = n.split('.')

    if(n[1]) {
        if(n[1].length == 1)
            n[1] += '0'
    } else
        n[1] = '00'
    return n[0] + '.' + n[1]
}

String.prototype.reverse = function(){ return this.split("").reverse().join(""); }
String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.split(search).join(replacement);
};
Element.prototype.css = function(ob){
	for(var i in ob)
		this.style[i] = ob[i];
	return this;
}

Element.prototype.append = function(html){    
	if(typeof html === 'string'){
		this.innerHTML += html; 
	} else { 
		this.appendChild(html)
	}
	return this;
}

Element.prototype.removeEv = function(ev, func){ this.removeEventListener(ev, func); return this; }
Element.prototype.html = function(txt){
	if(typeof txt == 'undefined') {
		return this.innerHTML; 
	} else {
		this.innerHTML = txt; 
		return this;
	}
}
Element.prototype.parent = function(i){   return this.parentNode; }
Element.prototype.find = function(i){     return this.querySelector(i); }
Element.prototype.findAll = function(i){  return this.querySelectorAll(i); }
Element.prototype.show = function(){             this.css({ display: "block" }); return this; }
Element.prototype.hide = function(){             this.css({ display: "none" });  return this; }
Element.prototype.class = function(i){           this.className += ' '+i; return this; }
Element.prototype.removeClass = function(i){     this.className = this.className.replaceAll(i,'').trim(); return this; }
Element.prototype.removeAttr  = function(array){ for(var i=0, il=array.length; i<il; i++){ this.removeAttribute(array[i]); } return this; }
Element.prototype.isHidden = function(){  return this.offsetParent === null; }
Element.prototype.on       = function(on, func){ this.addEventListener(on, func); return this; }
Element.prototype.showHide = function(){         this.isHidden() ? this.show() : this.hide(); return this;  }
Element.prototype.hasClass = function(c){ return new RegExp('(\\s|^)'+ c +'(\\s|$)').test(this.className); }
Element.prototype.attr = function(e){
	if(typeof e != 'object' && typeof e == 'string') {
		return this.getAttribute(e);
	} else {
		for(var i in e) this.setAttribute(i, e[i]);
		return this;
	}
}

$id = function(id_name){
	var o = document.getElementById(id_name);
	if(!o) return; return o;
}
$class = function(class_name, arr){
	var o = document.getElementsByClassName(class_name);
	if(!arr) return o[0]; else return o;
}
$queryOne = function(i){
	var o = document.querySelector(i);
	if(!o) { /*console.log('objektas nerastas',i);*/ return; }
	return o;
}
$query = function(i){
	return document.querySelectorAll(i);
}
$tag = function (t){
	var tag = document.getElementsByTagName(t)
	return tag;
}
$nElem = function(e){
	return document.createElement(e)
}





log = function(){
	for(var i=0,il=arguments.length;i<il;i++){
		console.log(arguments[i])
	}
}


Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

HTMLElement.prototype.appendFirst = function(childNode){
    if(this.firstChild)this.insertBefore(childNode,this.firstChild);
    else this.appendChild(childNode);
};


function pageInfo() {
	var parser = document.createElement('a');
		parser.href = location.href
	return {
		'protocols': parser.protocol, // => "http:"
		'host':      parser.host,     // => "example.com:3000"
		'hostname':  parser.hostname, // => "example.com"
		'port':      parser.port,     // => "3000"
		'pathname':  parser.pathname, // => "/pathname/"
		'hash':      parser.hash,     // => "#hash"
		'search':    parser.search,   // => "?search=test"
		'origin':    parser.origin    // => "http://example.com:3000"
	};
}

function js_post(url, data, resp, params) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if(resp){
				if(params.json){
					resp(JSON.parse(this.responseText));
				} else {
					resp(this.responseText);
				}
			}
        }
    };
	xhttp.timeout = 1000;
	
	if(params){
		if(params.timeout)
			xhttp.timeout = params.timeout
	}
	
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(data));
	
	return xhttp;
}
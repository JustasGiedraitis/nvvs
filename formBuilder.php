
<?php
	include_once 'sys/head.php';
	include_once 'sys/conn.php';
	include_once 'sys/post_worker.php';

    $__head_footer_loader->add_library('reportModal');
	
	$pattern = [];
	$pattern[] = 
	[
		[
			'row' => 'username', 
			'par' => [ 'label' => 'username'  ],
		],[
			'row' => 'password', 
			'par' => [  'label' => 'password' ], 
		],[
			'row' => 'address', 
			'par' => [ 'label' => 'address' ]	
		]
	];
	
	$pattern[] = 
	[
		[
			'row' => 'age',
			'par' => [ 'label' => 'age'  ],
		],[
			'row' => 'last_login',
			'par' => [ 'label' => 'last_login'  ],
		]
	];
	
	$pattern[] = 
	[
		[
			'row' => 'a1',
			'par' => [ 'label' => 'a1'  ],
		],[
			'row' => 'a1',
			'par' => [ 'label' => 'a2'  ],
		],[
			'row' => 'a3',
			'par' => [ 'label' => 'a3'  ],
		]
	];
	
	$pattern[] = 
	[
		[
			'row' => 'a4',
			'par' => [ 'label' => 'a4'  ],
		],[
			'row' => 'a5',
			'par' => [ 'label' => 'a5'  ],
		],[
			'row' => 'a6',
			'par' => [ 'label' => 'a6'  ],
		]
	];
	
	$pattern[] = 
	[
        [
            'row' => 'car_id',
            'row_id' => 'car_id',
            'par' => [ 'label' => 'test_dropdown', 'search' => 'cars_list' ]
        ] 
	];


	$table = 'users';
	$id_row = 'id';
	$elem = $db->prepare(' SELECT * FROM `'.$table.'` WHERE `'.$id_row.'` = :id ');
	$elem->bindParam(':id', $_REQUEST[$id_row]);
	$elem->execute();
	$result = $elem->fetch(PDO::FETCH_ASSOC);
?>

<form method='post' action="<?= $request ?>" onsubmit="submit_fix_values()" >
    <div class="container">
    <!-- <div class="container-fluid">-->

        <br><br>
        <h3>Vartotojai</h3><hr>

        <?php
            require_once 'sys/paprastas_formos_piesimas.php';
            /*
            foreach($pattern as $column) {
                echo "
                     <!-- --- --- --- --- row --- --- --- --- -->
                     <div class=\"col-xl-2  col-md-4  col-sm-6\">";
                foreach($column as $key => $params)
                {
                    $row_name = $params['row'];
                    $row_value = $result[$row_name];

                    $type = "text";
                    $label = "";
                    $search = "";
                    if($params['par']) {
                        if($params['par']['type'])
                            $type = $params['par']['type'];
                        if($params['par']['label'])
                            $label = $params['par']['label'];
                        if($params['par']['search'])
                            $search = $params['par']['search'];

                    }

                    echo '
                        <div class="form-group-sm">
                            <label for="id__'.$row_name.'">'.$label.'</label>
                                <div class="input-group mb-3">
                                  <input type="'.$type.'" name="0['. $row_name .']" id="id__'.$row_name.'" value="'. rmEmpty($row_value). '" column="'.$row_value.'" class="form-control form-control-sm" aria-label="" aria-describedby="basic-addon1" autocomplete="off">';

                                if($search){
                                    echo '

                                        <!-- extra -->
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" onclick="reportModal.show(\''.$search.'\')">
                                                <img src="assets/svg/list.svg" class="icon">
                                            </button>
                                        </div>';
                                }

                    echo '
                                </div>
                        </div>
                    ';
                }
                echo "</div>\n\n";
            }
            echo "                <!-- end rows -->\n";
            */
        ?>
    </div><!-- end container -->
	
	
    <div style="width: 1200px; margin:auto;">
			<br><br>
			<h5>Papildoma informacija:</h5>
			<div id='addRowDiv'></div>
	</div>
	<div style="width: 700px; margin:auto;">
			<br><br>
			<h5>Strukturizuota informacija:</h5>
			<div id='addRowDiv_h'></div>
	</div>
	
	
	
	
            <?php
                $elem = $db->prepare(' SELECT * FROM `extra` where user_id = :user_id ');
                $elem->bindParam(':user_id', $_REQUEST[$id_row]);
                $elem->execute();
                $result = $elem->fetchAll(PDO::FETCH_ASSOC);

				
				$elem = $db->prepare(' SELECT * FROM `axtre` where user_id = :user_id ');
                $elem->bindParam(':user_id', $_REQUEST[$id_row]);
                $elem->execute();
                $result2 = $elem->fetchAll(PDO::FETCH_ASSOC);
            ?>

			<script>
				addLineStructure = {
					options: {
						global_id_row: 'user_id',

						table: 'extra',
						id_row: 'id',
					},
					structure: 
					[
						{
							row: "username",
                            par: {
							    label: "username",
                                style: 'width:150px;'
							}
						},{
							row: "address",
                            par: {
							    label: "address",
                                style: 'width:150px;'
							}
						},{
							row: "age",
                            par: {
							    label: "age",
                                type: "number",
                                style: 'width:60px;'
                            },
						},{
							row: "last_login",
                            par: {
							    label: "last login",
								type: 'calendar',
                                style: 'width:100px;'
							}
						},{
							row: "a1",
                            par: {
							    label: "a1",
                                type: "number",
                                style: 'width:90px;'
                            }
						},{
							row: "a2",
                            par: {
							    label: "a2",
                                type: "number",
                                style: 'width:90px;'
                            }
						},{
                            row: "a3",
                            par: {
                                label: "a3",
                                type: "number",
                                style: 'width:90px;'
                            }
                        },{
                            row: "a4",
                            par: {
                                label: "a4",
                                type: "number",
                                style: 'width:90px;'
                            }
                        }
					]
				}
				
				addLineStructure2 = {
					options: {
						global_id_row: 'user_id',

						table: 'axtre',
						id_row: 'id',
					},
					structure: 
					[
						{
							row: "a1",
                            par: {
							    label: "a1",
                                type: "number",
                                style: 'width:90px;',
								rg: true
                            }
						},{
							row: "a2",
                            par: {
							    label: "a2",
                                type: "number",
                                style: 'width:90px;',
								rg: true
                            }
						},{
                            row: "a3",
                            par: {
                                label: "a3",
                                type: "number",
                                style: 'width:90px;',
								rg: true
                            }
                        },{
                            row: "a4",
                            par: {
                                label: "a4",
                                type: "number",
                                style: 'width:90px;',
								rg: true
                            }
                        }
					]
				}
				
				addLine(
					addRowDiv, 
					addLineStructure,
                    <?= json_encode($result) ?>
				)
				addLine(
					addRowDiv_h, 
					addLineStructure2,
                    <?= json_encode($result2) ?>
				)
			</script>	
			
			
			<script>			
			
				reportModal = (function(){
					var modalDialog;
					var modalIframe;
					var modalTitle;

					var modalAction = ''
                    var modalType = 'list'

                    var setIframe = function(){
                        modalDialog.find('.modal-body').append("<iframe src='/reportModal/"+modalAction+"/"+modalType+".php' style='border:none; width:100%;height:640px;display:block;' class='iframe-container'>")
                        modalIframe = modalDialog.find('iframe')
                    }


					var show = function(action,type){
						modalDialog = $id('modalReportDiv')
                        modalTitle = modalDialog.find('#modalTitle')

						if(!modalDialog.drawed || modalAction !== action) {
							modalDialog.drawed = true

                            modalType = type ? 'record' : 'list'
                            modalAction = action

                            setIframe()
						}

						$(modalDialog).modal('show');
					}


					var onReady = function(sendParts)
                    {
                        sendParts({
                            modalDialog: modalDialog,
                            modalIframe: modalIframe,
                            modalTitle:  modalTitle,
                        })
					}

					this.onReady = onReady;
					this.show = show;

					return this
                })();
			
			
			
			function submit_fix_values() {
				var input   = this.find('input')
				var select  = this.find('select')
				var txtArea = this.find('textarea')
				
				for(var i=0, il=input.length; i<il; i++){
					
				}
				for(var i=0, il=select.length; i<il; i++){
					
				}
				for(var i=0, il=txtArea.length; i<il; i++){
					
				}
			}
			
			</script>
			

			
			
			
			
			
			
			

			<br><br><br>

			<div style='display:block;text-align: center;'>
				<input type='submit' value='Išsaugoti pakeitimus' class='btn btn-primary'>
			</div>
			
			<br><br><br>
    
</form>










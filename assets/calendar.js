$(function(){
			
			calendar = (function(){
				// inputu kurie palaiko kalendoriu
				var calendarList = $('.calendar')
				var inputObject = null;
				var calendarObject = null;
				
				function generateCalendar(time) {
					var now;
					if(time) {
						
					} else {
						now = moment();
					}
					
					var thisYear = +moment(now).format('YYYY');
					
					// šiandien
					var thisDay = +moment(now).format('DD');
					
					
					// pilnas mėnuo nuo iki
					var m_s = moment(now).startOf('month')
					var m_e = moment(now).endOf('month')
					
					// mėnuo su su pilnom savaitėm
					var m_s_w = m_s.startOf('isoWeek')
					var m_e_w = m_e.endOf('isoWeek')
					
					// mėnuo
					var thisMonth = +m_s.format('MM') +1
					
					var days = m_e_w.diff(m_s_w, 'days')+1;
					var weeks = days / 7;
					
					var html = '';
					html += "\n<tr>"
						html += "\n<th class='calendar-title'></th>"
						html += "\n<th class='calendar-title'>P</th>"
						html += "\n<th class='calendar-title'>A</th>"
						html += "\n<th class='calendar-title'>T</th>"
						html += "\n<th class='calendar-title'>K</th>"
						html += "\n<th class='calendar-title'>Pn</th>"
						html += "\n<th class='calendar-title'>Š</th>"
						html += "\n<th class='calendar-title'>S</th>"
					html += "\n</tr>"
					
					for(var w=0; w<weeks; w++) {
						html += "\n<tr>\n";
						
						for(var d=0; d<7; d++) {
							var nd = moment(+m_s_w);
								nd.add(d +(w*7), 'days'); // nauja menesio diena
															
							var c_m = +nd.format('MM'); // naujas menuo					
							var addClass = 'calendar-c-day'
							if(c_m !== thisMonth)
								addClass = 'calendar-x-day'
							else {
								if(thisDay == +nd.format('DD')){
									addClass += ' calendar-today'
								}
							}	
							
							var week = nd.isoWeek();
							if(d == 0) {
								html += '<td class="calendar-week" onmousedown="calendar.set_input_week('+thisYear+','+week+').remove()">'+ week +'</td>'+"\n"
							}
							
							html += '<td class="'+addClass+'" onmousedown="calendar.set_input_day('+ nd.format('YYYY,MM,DD') +').remove()" >'+ nd.format('DD') +'</td>'+"\n"
						}
						html += '</tr>';
					}
					
					html = "<table class='unselectable'>" + 
						"<tr>"+
							"<th class='calendar-hd'></th>"+
							"<th colspan='4' class='calendar-hd'> << "+ thisYear +" >> </th>"+
							"<th colspan='3' class='calendar-hd'> << kovas >> </th>"+
						"</tr>" +
						html + 
					"</table>"

					return '<div id="calendar-table">' + html + '</div>';
				}
				
				
				function mousedownevent(e) {
					if ( $(e.target).closest(inputObject).length === 0 && 
						 $(e.target).closest(calendarObject).length ===0
					){
						remove();
					}
				}
				function keydownevent(r) {
					if(r.keyCode == 9) {
						remove();
					}
				}
				function showCalendar(element) {
					inputObject = element;
					
					
					var table = generateCalendar(this.value);
					calendarObject = $(table)
					/*
					var position = $(inputObject).position()
					calendarObject.css({ 
						top:  position.top + $(inputObject).height()+15, 
						left: position.left 
					})
					*/
					$(inputObject).parent().append(calendarObject);
					
					$(document).mousedown(mousedownevent);
					$(document).keydown(keydownevent);
					
				}
				function remove(){
					$(document).unbind('mousedown', keydownevent);
					$(document).unbind('keydown', mousedownevent);
					calendarObject.remove();
				}
				calendarList.on('focusin', function(){ showCalendar(this); });
				
				this.inputObject = inputObject;
				this.calendarList = calendarList;
				this.calendarObject = calendarObject;
				this.showCalendar = showCalendar;
				this.remove = remove;
				
				// custom
				this.set_input_week = function(year,week){
					console.log(year,week);
					return this;
				}
				this.set_input_day = function(year,month,day){
					console.log(year,month,day);
					inputObject.value = year +'-'+ month +'-'+ day
					return this;
				}
				
				return this;
				
			})();
		
})
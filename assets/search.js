var no_ajax_arrays = [];

/**
 * params {
 *      jei ne custom:
 *          table, row, limit (default 25), no_ajax,
 *          min_length (default 0 [double click atidaro sarasas], jei 1 - tik tada issoka sarasas)
 *
 *      custom:
 *          nurodyti failo vieta { custom: '..file..' }
 * }
*/
function search(self, call) {
    if(!self.is_ready) {

	
		var div = $nElem('div').css({ display:'none' }).html('<div class="search-menu dropdown-menu" style="display:block;"></div>');
		
		 self.parent().append(div)
		 self.search_div = div
	
	
		function inp_event(e) {			
			self.search_div.show().find('.dropdown-menu').html('<span class="dropdown-item"><img src=\'https://zippy.gfycat.com/MilkyLateDuiker.gif\' style="height:14px;margin-top: -4px;"></span>');
			
			if(self.value.length == 0) {
				if(self.ajax)
					self.ajax.abort();
				clearTimeout(self.timeout)
				self.search_div.hide()
				return;
			}
			
			clearTimeout(self.timeout)
			self.timeout = setTimeout(function(){
				if(self.ajax)
					self.ajax.abort();
				
				self.ajax = js_post('/search.php', { 
					call:   call, 
					val:    self.value.trim()
				}, function (e) { 
					
					if(e.length == 0){
						self.search_div.find('.dropdown-menu').html('<span class="dropdown-item">no result..</span>');
					} else {
						self.search_div.show();
						self.result = e;
						
						var rslt = '';
						for(var i=0, il=e.length; i<il; i++) {
							var active = '';
							if(i==0){
								active = 'active'
							}
							rslt += '<button class="dropdown-item '+active+'" type="button" i='+i+' >'+ e[i].f +'</button>'
						}
						
						var buttons = self.search_div.find('.dropdown-menu').html(rslt).findAll('button')
						for(var i=0, il=buttons.length; i<il; i++){
							buttons[i].on('mousedown',function(){
								self.search_div.hide()
								
								self.value = self.result[this.attr('i')].f
								self.attr({ value: self.result[this.attr('i')].id }) 
							});
							buttons[i].on('focus',function(){
								
							});
						}
					}
					
				},{ json: true });

			}, 400)
		}
		self.on('input', inp_event)
		self.on('keydown',function(e) {
			function activeIndex(drp) {
				for(var i=0, il=drp.length; i<il; i++)
					if(drp[i].hasClass('active'))
						return i;
			}
			
			
			if(e.key === 'ArrowUp') {
				var drp = self.search_div.findAll('.dropdown-menu .dropdown-item')
				var i = activeIndex(drp);
				if(i > 0) {
					drp[i].removeClass('active')
					drp[i-1].class('active')
				}
			}
			
			if(e.key === 'ArrowDown') {
				var drp = self.search_div.findAll('.dropdown-menu .dropdown-item')
				var i = activeIndex(drp);
				if(i < drp.length-1) {
					drp[i].removeClass('active')
					drp[i+1].class('active')
				}
			}
			
			if(e.key === 'Enter') {
				e.preventDefault()
				
				var drp = self.search_div.findAll('.dropdown-menu .dropdown-item')
				var i = activeIndex(drp);
				
				self.value = self.result[ i ].f
				self.attr({ value: self.result[ i ].id }) 
				self.search_div.hide()
			}
		})

        self.is_ready = true;
    }
}